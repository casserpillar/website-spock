{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Monoid
import Web.Spock.Safe
import Data.Text
import Data.Text.IO
import System.Environment
import Control.Monad.IO.Class

main :: IO ()
main =
    getArgs >>= \ arg ->
    runSpock (read (Prelude.head arg) :: Int) $ spockT id $
    do  get root $
            rootFiller
        
        get ("hello" <//> var) $ 
            helloFiller

        get ("ship" <//> var <//> var) $ 
            shipFiller

        get ("resume") $ 
            resumeFiller

        get ("wwcd") $
            wwcdFiller 

        get ("invitation" <//> var) $
            invitationFiller 

        get ("replace" <//> var) $
            replaceFiller     

rootFiller = html $ mconcat
    [ "<h1>Cass Fantastic</h1>"
    , "<p>Hi! This is Cassandra Fantastic's lil website, fyi.</p>"
    , "<p>Things:</p>"
    , "<ul><li><a href=/ship/x/y>Be gay!</a></li>"
    , "<li><a href=/resume>Hire me!</a></li>"
    , "</ul>"
    , "<a href=https://gitlab.com/casserpillar/website-spock/>Source for this thing!</a>"
    ]
    
helloFiller x = text ("Hello " <> x <> "!")

shipFiller x y = text (x <> " is gay for " <> y <> "!\nChange " <> x <> " and " <> y <> " in the URL to have fun!")

resumeFiller = text ("employ me")

wwcdFiller = text ("Cass would get takeout and snuggles")

invitationFiller :: MonadIO m => Text -> ActionCtxT ctx m ()
invitationFiller inviteName = do
    htmlText <- liftIO $ getHTMLasText invitationHTMLPath
    html $ Data.Text.replace "$name" cleanName htmlText
        where cleanName = Data.Text.replace ">" "" $
                          Data.Text.replace "<" "" inviteName

getHTMLasText :: FilePath -> IO Text
getHTMLasText path = 
    Data.Text.IO.readFile path
    
invitationHTMLPath = "./html/invitation.html"

replaceHTMLPath = "./html/replgayce.html" 

replaceFiller :: MonadIO m => Text -> ActionCtxT ctx m ()
replaceFiller replaceText = do
    htmlText <- liftIO $ getHTMLasText replaceHTMLPath
    html $ Data.Text.replace "$content" (gayUpdate cleanText) htmlText
        where cleanText = Data.Text.replace ">" "" $
                          Data.Text.replace "<" "" replaceText

gayUpdate :: Text -> Text
gayUpdate textToGay
    | textToGay == "" = ""
    | otherwise       = replace "a" "<span class=bigger>gay</span>" textToGay

                          {-but 
hi this is a comment 

how does this work

start at line 10

get root ~> ~/
    text "Hello World!"
    prints the html text Hello World when you visit /, root

get ### 
    takes a custom URL and returns content based on the URL, in this case using the
    var as a thing ~ /hello/var -> "Hello var", but can be, kiiiinda generic

so let's think about this

we can use this to make, a couple of different subpages kinda trivially, and have 
them grab content from a DB somehow, and display it? 

and then use some sort of styler to like, style it

this should work

--have like, a function for each panel on the site

and then generate divs in the central function that have content from the panel functions

and then and generate content for the panels based on pulling from a DB if necessary, or 
just like a list of text files in markdown


-}